# Generated by Django 4.0.4 on 2022-05-10 19:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apirest', '0005_product_rename_drink_quantity_order_quantity_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.CharField(choices=[('food', 'Plates'), ('food', 'Soups'), ('drink', 'Drinks'), ('drink', 'Cocktails'), ('drink', 'Beers')], default=1, max_length=5),
            preserve_default=False,
        ),
    ]
