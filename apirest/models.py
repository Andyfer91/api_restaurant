from django.db import models

# Create your models here.

""" Plates and drinks depends on Order from the user"""


class Product(models.Model):
    """
    Manage to register drinks
    """
    FOOD_CATEGORY = [
        ('plate', 'Plates'),
        ('soup', 'Soups'),
        ('drink', 'Drinks'),
        ('cocktail', 'Cocktails'),
        ('beer', 'Beers')
    ]
    product_name = models.CharField(max_length=45)
    stock = models.IntegerField()
    cost = models.IntegerField()
    category = models.CharField(choices=FOOD_CATEGORY, max_length=8)

    def __str__(self):
        return self.product_name


class Table(models.Model):
    """
    Manage to control quantity and state of tabbles
    """
    clients_cant = models.IntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return self.location


class Client(models.Model):
    """
    Relevant information about the client
    """
    WAYS_TO_PAY = (
        ('C', 'Currency'),
        ('CC', 'Credit Card'),
        ('SA', 'Save Account'),
    )
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    pay_way = models.CharField(choices=WAYS_TO_PAY,max_length=2)
    observations = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return self.name + " " + self.last_name


class Waiter(models.Model):

    id_waiter = models.AutoField(primary_key=True, default=False)
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    last_name_two = models.CharField(max_length=45)

    def __str__(self):
        return self.name + " " + self.last_name


class Bill(models.Model):
    """
    Manage to register all the orders that client makes to the restaurant
    """
    id_bill = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now=True)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE, null=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=True)
    table = models.ForeignKey(Table, on_delete=models.CASCADE, null=True)
    close_bill = models.BooleanField(default=False)

    def __str__(self):
        return "Bill No " + f'{self.id_bill} '


class Order(models.Model):
    """
    Manage to register every order de client require
    """
    id_order = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    bill_id = models.ForeignKey(Bill, related_name='orders', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id_order} ' + self.product.product_name
