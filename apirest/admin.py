from django.contrib import admin
from .models import Client, Waiter, Table, Product, Bill, Order

# Register your models here.

admin.site.register(Client)
admin.site.register(Waiter)
admin.site.register(Table)
admin.site.register(Product)
admin.site.register(Bill)
admin.site.register(Order)
