from django.urls import path, include
from rest_framework import routers

from .models import Client, Product, Bill, Waiter, Table
from .serializer_views import ProductViewSet, TableViewSet, WaiterViewSet, ClientViewSet, BillViewSet, OrderViewSet
from .serializers import ProductSerializer
from .views import ProductList, ProductCreate, ProductUpdate, ProductDelete, \
    BillList, OrderList, BillCreate, BillDelete, BillUpdate, \
    OrderCreate, OrderDelete, OrderUpdate, \
    ClientCreate, home, BillDetail

router=routers.DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'tables', TableViewSet)
router.register(r'Waiters', WaiterViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'bills', BillViewSet)
router.register(r'orders', OrderViewSet)

urlpatterns = [
    path('', home, name="home"),

    path('list', ProductList.as_view(), name='list'),
    path('create', ProductCreate.as_view(), name='create'),
    path('update/<int:pk>', ProductUpdate.as_view(), name='update'),
    path('delete/<int:pk>', ProductDelete.as_view(), name='delete'),

    path('bill-list', BillList.as_view(), name='bills-list'),
    path('bill-create', BillCreate.as_view(), name='bill-create'),
    path('bill-delete/<int:pk>', BillDelete.as_view(), name='bill-delete'),
    path('bill-update/<int:pk>', BillUpdate.as_view(),name='bill-update'),
    path('bill-datail/<int:pk>', BillDetail.as_view(), name='bill-detail'),

    path('order-list', OrderList.as_view(), name='orders-list'),
    path('order-create', OrderCreate.as_view(), name='order-create'),
    path('order-delete/<int:pk>', OrderDelete.as_view(), name='order-delete'),
    path('order-update/<int:pk>', OrderUpdate.as_view(), name='order-update'),

    path('client-create', ClientCreate.as_view(), name='client-create')


]