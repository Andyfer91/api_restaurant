from abc import ABC

from rest_framework import serializers
from .models import Product, Table, Waiter, Client, Bill, Order


class ProductSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Product
        fields = ['product_name', 'stock', 'cost', 'category']


class TableSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Table
        fields = ['url', 'client_cant', 'location']


class ClientSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Client
        URL_FIELD_NAME = ['mesita']
        fields = '__all__'


class WaiterSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Waiter
        fields = '__all__'


class BillSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Bill
        URL_FIELD_NAME = ['factura']
        fields = ['id_bill', 'date', 'waiter', 'client', 'table', 'close_bill', 'orders']


class OrderSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'