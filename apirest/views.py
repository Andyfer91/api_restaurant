from django.shortcuts import render

# Create your views here.
from django.views import View
from django.views.generic import ListView, CreateView, DeleteView, UpdateView, DetailView
from apirest.models import Product, Bill, Order, Client, Table, Waiter


# Product Model CRUD
class ProductList(ListView):
    model = Product
    template_name = "products/list.html"
    queryset = Product.objects.all()
    context_object_name = 'product_list'
    extra_context = {'msg': 'se cargado correctamente'}


class ProductCreate(CreateView):
    model = Product
    template_name = "products/create.html"
    fields = ['product_name', 'stock','cost', 'category']
    extra_context = {'msg': 'se creado correctamente'}
    success_url = "/list"


class ProductUpdate(UpdateView):
    model = Product
    fields = ['product_name', 'stock', 'cost', 'category']
    pk_url_kwarg = 'pk'
    template_name = "products/update.html"
    success_url = "/list"
    context_object_name = "product_update"


class ProductDelete(DeleteView):
    model = Product
    fields = ['product_name', 'stock', 'cost', 'category']
    pk_url_kwarg = 'pk'
    template_name = "products/delete.html"
    success_url = "/list"
    context_object_name = "product_delete"
    extra_context = {'msg': 'object deleted'}


# Bill Model CRUD
class BillList(ListView):
    model = Bill
    template_name = "bills/bill-list.html"
    queryset = Bill.objects.filter(close_bill=False)
    context_object_name = 'bill_list'
    extra_context = {'msg': "Para servirte"}


class BillDetail(DetailView):
    model = Bill
    template_name = "bills/bill-detail.html"
    pk_url_kwarg = "pk"
    fields = ['waiter', 'client', 'table','close_bill']
    context_object_name = "bill_detail"


class BillCreate(CreateView):
    model = Bill
    template_name = "bills/create-bill.html"
    fields = ['waiter', 'client', 'table']
    extra_context = {'msg': 'se creado correctamente'}
    success_url = "/bill-list"


class BillUpdate(UpdateView):
    model = Bill
    fields = ['waiter', 'client', 'table','close_bill']
    pk_url_kwarg = 'pk'
    template_name = "bills/update-bill.html"
    success_url = "/bill-list"
    context_object_name = "bill_update"


class BillDelete(DeleteView):
    model = Bill
    fields = ['id_bill', 'date','waiter', 'client', 'table']
    pk_url_kwarg = 'pk'
    template_name = "bills/delete-bill.html"
    success_url = "/bill-list"
    context_object_name = "bill_delete"
    extra_context = {'msg': 'ningun mensaje'}


# Order Model CRUD
class OrderList(ListView):
    model = Order
    queryset = Order.objects.all()
    template_name = "orders/order-list.html"
    context_object_name = "orders"


class OrderCreate(CreateView):
    model = Order
    template_name = "orders/create-order.html"
    fields = ['product','quantity', 'bill_id']
    extra_context = {'msg': 'se creado correctamente'}
    success_url = "/order-list"


class OrderUpdate(UpdateView):
    model = Order
    fields = ['product','quantity', 'bill_id']
    pk_url_kwarg = 'pk'
    template_name = "orders/update-order.html"
    success_url = "/order-list"
    context_object_name = "order_update"


class OrderDelete(DeleteView):
    model = Order
    fields = ['product','quantity', 'bill_id']
    pk_url_kwarg = 'pk'
    template_name = "orders/delete-order.html"
    success_url = "/order-list"
    context_object_name = "order_delete"
    extra_context = {'msg': 'object deleted'}


# Client Model CRUD
class ClientList(ListView):
    model = Client
    queryset = Client.objects.all()
    template_name = "clients/order-list.html"
    context_object_name = "clients"


class ClientCreate(CreateView):
    model = Client
    template_name = "clients/create-client.html"
    fields = ['name','last_name', 'pay_way', 'observations']
    extra_context = {'msg': 'se creado correctamente'}
    success_url = "/bill-create"


class ClientUpdate(UpdateView):
    model = Client
    fields = ['name','last_name', 'pay_way', 'observations']
    pk_url_kwarg = 'pk'
    template_name = "clients/update-client.html"
    success_url = "/client_list"
    context_object_name = "client_update"


class ClientDelete(DeleteView):
    model = Client
    fields = ['name','last_name', 'pay_way', 'observations']
    pk_url_kwarg = 'pk'
    template_name = "clients/delete-client.html"
    success_url = "/client_list"
    context_object_name = "product_delete"
    extra_context = {'msg': 'object deleted'}


# Order Model CRUD
class TableList(ListView):
    model = Table
    queryset = Table.objects.all()
    template_name = "tables/table-list.html"
    context_object_name = "tables"


class TableCreate(CreateView):
    model = Table
    template_name = "tables/create-table.html"
    fields = ['clients_cant','location']
    extra_context = {'msg': 'se creado correctamente'}
    success_url = "/table_list"


class TableUpdate(UpdateView):
    model = Table
    fields = ['clients_cant','location']
    pk_url_kwarg = 'pk'
    template_name = "tables/update-table.html"
    success_url = "/table_list"
    context_object_name = "table_update"


class TableDelete(DeleteView):
    model = Table
    fields = ['clients_cant','location']
    pk_url_kwarg = 'pk'
    template_name = "tables/delete-table.html"
    success_url = "/table_list"
    context_object_name = "table_delete"
    extra_context = {'msg': 'object deleted'}


# Waiter Model CRUD
class WaiterList(ListView):
    model = Waiter
    queryset = Waiter.objects.all()
    template_name = "waiters/waiter-list.html"
    context_object_name = "waiters"


class WaiterCreate(CreateView):
    model = Waiter
    template_name = "waiters/create-waiter.html"
    fields = ['name','last_name', 'last_name_two']
    extra_context = {'msg': 'se creado correctamente'}
    success_url = "/waiter_list"


class WaiterUpdate(UpdateView):
    model = Waiter
    fields = ['name','last_name', 'last_name_two']
    pk_url_kwarg = 'pk'
    template_name = "waiters/update-waiter.html"
    success_url = "/waiter_list"
    context_object_name = "waiter_update"


class WaiterDelete(DeleteView):
    model = Waiter
    fields = ['name','last_name', 'last_name_two']
    pk_url_kwarg = 'pk'
    template_name = "waiters/delete-waiter.html"
    success_url = "/waiter_list"
    context_object_name = "waiter_delete"
    extra_context = {'msg': 'object deleted'}


def home(request):
    return render(request, 'base/base.html')